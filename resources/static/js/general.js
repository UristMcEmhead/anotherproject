function loadCSS(path) {
    var link = document.createElement("link");
    link.rel = "stylesheet";
    link.href = path + ".css";

    console.log("reloading " + link.href);
    document.getElementsByTagName("head")[0].appendChild(link);
}

function reloadCSS(theme) {
    loadCSS("/css/bootstrap.min." + theme);
    loadCSS("/css/" + theme);
    loadCSS("/css/general");
}

function changeTheme() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            console.log("toggle theme response: '" + this.responseText + "'");
            reloadCSS(this.responseText);
        }
    };

    request.open('POST', '/theme/change', true);
    request.send();

    // remove focus from button
    document.getElementById("theme-button").blur();
}
