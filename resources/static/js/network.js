var minHeight = 250;
var padding = 20;

function resizeNetwork() {
    var div = document.getElementById("network");
    var box = div.getBoundingClientRect();

    var fullHeight = window.innerHeight - (box.top + padding);
    var height = Math.max(minHeight, fullHeight);

    div.style.height = height + "px";
}

function initNetworkDiv() {
     resizeNetwork();
     window.onresize = resizeNetwork;
}
