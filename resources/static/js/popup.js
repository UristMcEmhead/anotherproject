var padding = 15;

function correctPosition(popup) {
    var container = popup.parentElement;

    var cRect = container.getBoundingClientRect();
    var pRect = popup.getBoundingClientRect();

    var rawLeft = (cRect.width / 2) - (pRect.width / 2);
    var minLeft = padding - cRect.left;
    var maxLeft = window.innerWidth - (cRect.left + pRect.width + padding);

    popup.style.left = Math.min(maxLeft, Math.max(minLeft, rawLeft)) + 'px';
}

function setupPopup(popup) {
    var container = popup.parentElement;

    container.onmouseover = function() {
        popup.style.visibility = 'visible';
    };

    container.onmouseout = function() {
        popup.style.visibility = 'hidden';
    };
}

function setupPopups() {
    var popups = document.querySelectorAll('.popup');
    popups.forEach(setupPopup);
    popups.forEach(correctPosition);

    window.onresize = function () {
        popups.forEach(correctPosition);
    }
}
