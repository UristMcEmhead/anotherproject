<#-- @ftlvariable name="file" type="ru.itis.wiki.dto.PopupFileDto" -->
<div class="popup-container">
    <a class="${file.valid?then('valid', 'invalid')}" href="/file/${file.ref}">%link_text</a>
    <#if file.desc??>
        <div class="popup card border-primary">
            <div class="card-header">${file.name}</div>
            <div class="card-body">${file.desc}</div>
        </div>
    </#if>
</div>