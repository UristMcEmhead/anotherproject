<#-- @ftlvariable name="dir" type="ru.itis.wiki.models.Directory" -->
<#-- @ftlvariable name="file" type="ru.itis.wiki.dto.FileDto" -->
<#import "lib/html.ftl" as H>

<#assign new = !(file.name)??>

<@H.html>
    <@H.head (new?string("Create", "Edit") + " file")/>
    <@H.body>
        <form id="theForm" action="/file/${new?string("create", "edit")}/${(dir.id)!""}" method="post">
            <div class="formGroup">
                <label for="name">Name:</label>
                <input type="text" class="form-control" id="name" value="${(file.name)!""}" ${new?string("", "readonly")} name="name">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                <label for="desc">Description:</label>
                <textarea class="form-control" rows="4" id="desc" form="theForm" name="description">${(file.description)!""}</textarea>

                <label for="body">Content</label>
                <textarea class="form-control" rows="14" id="body" form="theForm" name="body">${(file.body)!""}</textarea>

                <br>
                <input type="submit" value="Submit" class="btn btn-primary">
                <a href="${new?then("/index/" + (dir.id)!"", "/file/" + file.name)}" class="btn btn-secondary">Cancel</a>
            </div>
        </form>
    </@H.body>
</@H.html>
