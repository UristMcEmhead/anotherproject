<#-- @ftlvariable name="Session.theme" type="java.lang.String" -->
<#-- @ftlvariable name="Session.me" type="ru.itis.wiki.dto.UserDto" -->
<#macro html>
    <!DOCTYPE html>
    <html lang="en">
        <#nested>
    </html>
</#macro>

<#macro head title>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>${title}</title>

        <#assign theme = Session.theme!"dark">
        <#if theme == "dark">
            <link rel="stylesheet" href="/css/bootstrap.min.dark.css">
            <link rel="stylesheet" href="/css/dark.css">
        <#else>
            <link rel="stylesheet" href="/css/bootstrap.min.light.css">
            <link rel="stylesheet" href="/css/light.css">
        </#if>
        <link rel="stylesheet" href="/css/general.css">
        <script src="/js/jquery-3.4.1.min.js"></script>
        <script src="/js/bootstrap.bundle.min.js"></script>
        <script src="https://kit.fontawesome.com/144a622e90.js" crossorigin="anonymous"></script>
        <script src="/js/general.js"></script>
        <#nested>
    </head>
</#macro>

<#macro body onload = "">
    <body onload="${onload}">
    <div class="navbar navbar-expand-lg position-static navbar-dark bg-secondary">
        <div class="container">
            <a class="navbar-brand" href="/index">WikiMap</a>
            <ul class="navbar-nav ml-auto">
                <#if (Session.me)??>
                    <#-- "me" is present in the session -->
                    <li class="nav-item">
                        <a class="btn btn-secondary" href="/user/${Session.me.username}">${Session.me.username}</a>
                    </li>
                <#else>
                    <#-- "me" is not present in the session -->
                    <li class="nav-item">
                        <a data-toggle="modal" class="btn btn-secondary" href="#sign-in-modal">Sign in</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="modal" class="btn btn-secondary" href="#sign-up-modal">Sign up</a>
                    </li>
                </#if>
                <li class="nav-item dropdown dropdown-menu-right">
                    <button class="btn btn-secondary" id="navbarDropdown" data-toggle="dropdown">
                        <i class="fas fa-bars"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <button id="theme-button" class="dropdown-item" onclick="changeTheme()">
                            Toggle theme
                        </button>
                        <#if (Session.me)??>
                            <a href="/signout" class="dropdown-item">Sign out</a>
                        </#if>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
        <#nested>
        <#if !(Session.me)??>
            <#-- sign in modal -->
            <div class="modal fade" id="sign-in-modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>
                            <button type="button" class="close" data-dismiss="modal">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                        <form action="/signin" method="POST">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label data-error="wrong" data-success="right" for="in-username">Username</label>
                                    <input type="text" id="in-username" name="username" class="form-control validate">
                                </div>
                                <div class="form-group">
                                    <label data-error="wrong" data-success="right" for="in-password">Password</label>
                                    <input type="password" id="in-password" name="password" class="form-control validate">
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <input type="submit" class="btn btn-primary" value="Sign in">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <#-- sign up modal -->
            <div class="modal fade" id="sign-up-modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold">Sign up</h4>
                            <button type="button" class="close" data-dismiss="modal">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                        <form action="/signup" method="POST">
                            <div class="modal-body">
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                                <div class="form-group">
                                    <label data-error="wrong" data-success="right" for="up-username">Username</label>
                                    <input type="text" id="up-username" name="username" class="form-control validate">
                                </div>
                                <div class="form-group">
                                    <label data-error="wrong" data-success="right" for="up-password">Password</label>
                                    <input type="password" id="up-password" name="password" class="form-control validate">
                                </div>
                                <div class="form-group">
                                    <label data-error="wrong" data-success="right" for="up-password-again">Repeat password</label>
                                    <input type="password" id="up-password-again" name="passwordAgain" class="form-control validate">
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <input type="submit" class="btn btn-primary" value="Sign up">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </#if>
    </div>
    </body>
</#macro>

<#macro textAndDropdown text>
    <div class="card border-0 page-header bg-transparent">
        <div class="card-body">
            <div class="page-text">
                <pre>${text}</pre>
            </div>
            <div class="dropdown float-right">
                <button class="btn btn-outline-secondary" id="pageHeaderDropdown" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="pageHeaderDropdown">
                    <#nested>
                </div>
            </div>
        </div>
    </div>
    <hr>
</#macro>
