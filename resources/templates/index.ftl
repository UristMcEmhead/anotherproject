<#-- @ftlvariable name="Session.me" type="ru.itis.wiki.dto.UserDto" -->
<#-- @ftlvariable name="curr_dir_path" type="java.lang.String" -->
<#-- @ftlvariable name="curr_dir" type="ru.itis.wiki.models.Directory" -->
<#-- @ftlvariable name="dirs" type="java.util.Collection<ru.itis.wiki.models.Directory>" -->
<#-- @ftlvariable name="files" type="java.util.Collection<ru.itis.wiki.dto.IndexFileDto>" -->
<#import "lib/html.ftl" as H>

<@H.html>
    <@H.head "Index"/>
    <@H.body>
        <@H.textAndDropdown "Index of <code>${curr_dir_path}</code>">
            <a class="dropdown-item" href="/map/${(curr_dir.id)!""}">To map</a>
            <#if (Session.me)??>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="/dir/create?parent=${(curr_dir.id)!""}">Create directory</a>
                <a class="dropdown-item" href="/file/create/${(curr_dir.id)!""}">Create file</a>
                <#if curr_dir?? && Session.me.id == curr_dir.id>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/dir/${curr_dir.id}/edit">Edit directory</a>
                    <a class="dropdown-item" href="/dir/${curr_dir.id}/delete">Delete directory</a>
                </#if>
            </#if>
        </@H.textAndDropdown>
        <#if curr_dir??>
            <div>
                <a href="/index/${(curr_dir.parent.id) ! ""}">&lt; back</a>
            </div>
            <br>
        </#if>
        <#list dirs>
            <#items as dir>
                <div>
                    <a href="/index/${dir.id}">&gt; ${dir.name}</a>
                </div>
            </#items>
            <br>
        </#list>
        <#list files as file>
            <div>
                <a href="/file/${file.ref}">* ${file.name}</a>
            </div>
        </#list>
    </@H.body>
</@H.html>
