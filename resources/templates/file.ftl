<#-- @ftlvariable name="Session.me" type="ru.itis.wiki.dto.UserDto" -->
<#-- @ftlvariable name="file" type="ru.itis.wiki.dto.FileDto" -->
<#import "lib/html.ftl" as H>

<@H.html>
    <@H.head file.name>
        <script src="/js/popup.js"></script>
    </@H.head>
    <@H.body onload="setupPopups();">
        <#assign text><a href="/index/${(file.dir.id)!""}">Back to <code>${(file.dir.name)!"/"}</code></a></#assign>
        <@H.textAndDropdown text>
            <a href="/map/${(file.dir.id)!""}" class="dropdown-item">To map</a>
            <#if (Session.me)?? && Session.me.id == file.owner.id>
                <div class="dropdown-divider"></div>
                <a href="/file/${file.ref}/edit" class="dropdown-item">Edit file</a>
                <a href="/file/${file.ref}/delete" class="dropdown-item">Delete file</a>
            </#if>
        </@H.textAndDropdown>
        <h1>${file.name}</h1>
        <div class="file-desc">
            ${file.description}
        </div>
        <hr>
        <div class="file-body">
            ${file.body}
        </div>
    </@H.body>
</@H.html>
