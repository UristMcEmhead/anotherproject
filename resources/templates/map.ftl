<#-- @ftlvariable name="nodes" type="java.util.List<ru.itis.wiki.dto.MindMapNodeDto>" -->
<#-- @ftlvariable name="curr_dir_path" type="java.lang.String" -->
<#-- @ftlvariable name="curr_dir" type="ru.itis.wiki.models.Directory" -->
<#-- @ftlvariable name="Session.theme" type="java.lang.String" -->
<#import "lib/html.ftl" as H>

<@H.html>
<@H.head "Mind Map">
    <script src="/js/vis-network.min.js"></script>
    <script src="/js/network.js"></script>
</@H.head>
<@H.body>
    <@H.textAndDropdown "Map of <code>${curr_dir_path}</code>">
        <a class="dropdown-item" href="/index/${(curr_dir.id)!""}">To index</a>
    </@H.textAndDropdown>
    <div id="network" class="network border border-info"></div>
</@H.body>
</@H.html>
<script type="text/javascript">
    initNetworkDiv();

    var nodes = new vis.DataSet([
        <#list nodes as node>
        {id: ${node.id}, label: "${node.name}", group: ${node.group}},
        </#list>
    ]);

    var edges = new vis.DataSet([
        <#list nodes as node>
        <#list node.links as link>
        {from: ${node.id}, to: ${link}},
        </#list>
        </#list>
    ]);

    var container = document.getElementById('network');
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {
        nodes: {
            shape: 'dot',
            size: 20,
            <#assign darkTheme = (Session.theme!"dark") == "dark">
            <#assign light = "#fff">
            <#assign dark = "#222">
            font: {
                size: 20,
                strokeWidth: 4,
                color: '${darkTheme?then(light, dark)}',
                strokeColor: '${darkTheme?then(dark, light)}'
            },
            borderWidth: 5,
            physics: false
        },
        edges: {
            arrows: {
                to: {
                    enabled: true,
                    scaleFactor: 1
                }
            },
            width: 3,
            smooth: false
        }
    };
    var network = new vis.Network(container, data, options);
</script>
