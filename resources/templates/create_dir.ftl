<#-- @ftlvariable name="parent" type="ru.itis.wiki.models.Directory" -->
<#-- @ftlvariable name="dir" type="ru.itis.wiki.models.Directory" -->
<#import "lib/html.ftl" as H>

<#assign new = !(dir.id)??>

<@H.html>
    <@H.head (new?string("Create", "Edit") + " directory")/>
    <@H.body>
        <form action="/dir/create" method="post">
            <div class="formGroup">
                <label for="name">Name:</label>
                <input id="name" class="form-control" type="text" value="${(dir.name)!""}" name="name">

                <br>
                <input name="id" value="${(dir.id)!""}" hidden>
                <input name="parent" value="${(parent.id)!""}" hidden>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">

                <input type="submit" value="Submit" class="btn btn-info">
                <a href="${new?then("/index/" + (parent.id)!"", "/index/" + dir.id)}" class="btn btn-secondary">Cancel</a>
            </div>
        </form>
    </@H.body>
</@H.html>
