package ru.itis.wiki.controllers;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.itis.wiki.services.DirService;
import ru.itis.wiki.services.IndexService;

@Controller
@AllArgsConstructor
class IndexController {
    private final DirService dirService;
    private final IndexService indexService;

    @GetMapping("/index/{id}")
    public String indexOf(@PathVariable long id, ModelMap map) {
        var dir = dirService.get(id);
        var children = indexService.getChildrenOf(dir);

        map.put("dirs", children.getFirst());
        map.put("files", children.getSecond());
        map.put("curr_dir", dir);
        map.put("curr_dir_path", indexService.getPathTo(dir));

        return "index";
    }

    @GetMapping("/index")
    public String indexOfRoot(ModelMap map) {
        var children = indexService.getChildrenOf(null);

        map.put("dirs", children.getFirst());
        map.put("files", children.getSecond());
        map.put("curr_dir_path", "/");

        return "index";
    }

    @GetMapping("/")
    public String toIndex() {
        return "redirect:/index";
    }
}
