package ru.itis.wiki.controllers;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.itis.wiki.services.DirService;
import ru.itis.wiki.services.IndexService;
import ru.itis.wiki.services.MindMapService;

@Controller
@AllArgsConstructor
class MindMapController {
    private final DirService dirService;
    private final MindMapService mindMapService;
    private final IndexService indexService;

    @GetMapping("/map")
    public String getMap(ModelMap modelMap) {
        modelMap.put("curr_dir_path", "/");
        modelMap.put("nodes", mindMapService.getNodes(null));

        return "map";
    }

    @GetMapping("/map/{dirId}")
    public String getMap(@PathVariable long dirId, ModelMap modelMap) {
        var dir = dirService.get(dirId);
        modelMap.put("curr_dir", dir);
        modelMap.put("curr_dir_path", indexService.getPathTo(dir));
        modelMap.put("nodes", mindMapService.getNodes(dir));

        return "map";
    }
}
