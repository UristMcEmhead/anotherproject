package ru.itis.wiki.controllers;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@AllArgsConstructor
class UserController {

    @PostMapping("/theme/change")
    @ResponseBody
    public String setTheme(HttpServletRequest request, HttpServletResponse response) {
        var session = request.getSession();
        var oldTheme = session.getAttribute("theme");
        var newTheme = "dark".equals(oldTheme) ? "light" : "dark";

        var cookie = new Cookie("theme", newTheme);
        cookie.setMaxAge(630720000);    // approx 20 years
        cookie.setPath("/");

        session.setAttribute("theme", newTheme);
        response.addCookie(cookie);
        return newTheme;
    }
}
