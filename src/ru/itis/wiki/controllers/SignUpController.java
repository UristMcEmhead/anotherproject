package ru.itis.wiki.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;
import ru.itis.wiki.forms.SignUpForm;
import ru.itis.wiki.services.UserService;

@Controller
@AllArgsConstructor
class SignUpController {
    private final UserService userService;

    @PostMapping("/signup")
    public String signUp(SignUpForm form) {
        try {
            userService.signUp(form);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }

        System.out.println("forwarding to /signin");
        return "forward:/signin";
    }
}
