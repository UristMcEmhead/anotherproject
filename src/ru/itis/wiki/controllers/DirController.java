package ru.itis.wiki.controllers;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.wiki.forms.DirForm;
import ru.itis.wiki.services.DirService;
import ru.itis.wiki.services.ValidationService;

import javax.servlet.http.HttpSession;

@Controller
@AllArgsConstructor
class DirController {
    private final DirService dirService;
    private final ValidationService validationService;

    @GetMapping("/dir/create")
    public String getForm(@RequestParam Long parent, ModelMap map) {
        if (parent != null) {
            map.put("parent", dirService.get(parent));
        }

        return "create_dir";
    }

    @PostMapping("/dir/create")
    public String create(HttpSession session, DirForm form) {
        var dir = dirService.save(validationService.extractFrom(session), form);
        return "redirect:/index/" + dir.getId();
    }

    @GetMapping("/dir/{id}/edit")
    public String edit(@PathVariable long id, ModelMap map, HttpSession session) {
        var dir = dirService.get(id);
        validationService.assertIdEquals(session, dir.getOwner().getId());

        map.put("dir", dir);
        map.put("parent", dir.getParent());

        return "create_dir";
    }

    @GetMapping("/dir/{id}/delete")
    public String delete(@PathVariable long id, HttpSession session) {
        var dir = dirService.get(id);
        validationService.assertIdEquals(session, dir.getOwner().getId());

        var parent = dir.getParent();
        dirService.delete(dir);

        return "redirect:/index" + (parent == null ? "" : "/" + parent.getId());
    }
}
