package ru.itis.wiki.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.wiki.dto.FileDto;
import ru.itis.wiki.forms.FileForm;
import ru.itis.wiki.services.DirService;
import ru.itis.wiki.services.FileService;
import ru.itis.wiki.services.ValidationService;
import ru.itis.wiki.utils.Cache;
import ru.itis.wiki.utils.CacheManager;

import javax.servlet.http.HttpSession;

@Controller
class FileController {
    private final FileService fileService;
    private final DirService dirService;
    private final ValidationService validationService;
    private final Cache<String, FileDto> cache;

    public FileController(FileService fileService, DirService dirService, ValidationService validationService, CacheManager cacheManager) {
        this.fileService = fileService;
        this.dirService = dirService;
        this.validationService = validationService;
        this.cache = cacheManager.getCache("fileDto");
    }

    @GetMapping("/file/{name}")
    public String getFile(@PathVariable String name, ModelMap map) {
        map.put("file", cache.get(name.toLowerCase(), fileService::getDto));
        return "file";
    }

    @GetMapping("/file/create/{dirId}")
    public String getCreate(@PathVariable Long dirId, ModelMap map) {
        if (dirId != null) {
            map.put("dir", dirService.get(dirId));
        }

        return "create_file";
    }

    @GetMapping("/file/create")
    public String getCreate(ModelMap map) {
        return getCreate(null, map);
    }

    @GetMapping("/file/{name}/edit")
    public String getEdit(@PathVariable String name, ModelMap map, HttpSession session) {
        var file = fileService.getEditable(name);
        validationService.assertIdEquals(session, file.getOwner().getId());

        map.put("dir", file.getDirectory());
        map.put("file", file);

        return "create_file";
    }

    @PostMapping("/file/create/{dirId}")
    public String createFile(@PathVariable Long dirId, FileForm form, HttpSession session) {
        var file = fileService.create(validationService.extractFrom(session), form, dirId);
        return "redirect:/file/" + file.getRef();
    }

    @PostMapping("/file/create")
    public String createFile(FileForm form, HttpSession session) {
        return createFile(null, form, session);
    }

    @PostMapping("/file/edit/{dirId}")
    public String editFile(@PathVariable Long dirId, FileForm form, HttpSession session) {
        var file = fileService.update(validationService.extractFrom(session), form, dirId);
        cache.invalidateAll();

        return "redirect:/file/" + file.getRef();
    }

    @PostMapping("/file/edit")
    public String editFile(FileForm form, HttpSession session) {
        return editFile(null, form, session);
    }

    @GetMapping("/file/{name}/delete")
    public String delete(@PathVariable String name, HttpSession session) {
        var file = fileService.get(name);
        validationService.assertIdEquals(session, file.getOwner().getId());

        var dir = file.getDirectory();
        fileService.delete(file);

        return "redirect:/index/" + (dir == null ? "" : dir.getId());
    }
}
