package ru.itis.wiki.utils;

import java.util.Optional;
import java.util.function.Function;

public interface Cache<K, V> {

    Optional<V> get(K key);

    V get(K key, Function<? super K, ? extends V> keyToValueFunction);

    void put(K key, V value);

    void invalidate(K key);

    void invalidateAll();
}
