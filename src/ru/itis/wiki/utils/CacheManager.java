package ru.itis.wiki.utils;

public interface CacheManager {

    <K, V> Cache<K, V> getCache(String name);
}
