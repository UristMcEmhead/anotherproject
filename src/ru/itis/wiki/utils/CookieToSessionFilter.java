package ru.itis.wiki.utils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class CookieToSessionFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            var req = (HttpServletRequest) request;
            var session = req.getSession();
            var cookies = req.getCookies();

            if (session.getAttribute("theme") == null && cookies != null) {
                for (var cookie : cookies) {
                    if (cookie.getName().equals("theme")) {
                        session.setAttribute("theme", cookie.getValue());
                    }
                }
            }
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
