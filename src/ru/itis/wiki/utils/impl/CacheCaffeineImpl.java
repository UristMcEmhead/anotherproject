package ru.itis.wiki.utils.impl;

import com.github.benmanes.caffeine.cache.Caffeine;
import ru.itis.wiki.utils.Cache;

import java.util.Optional;
import java.util.function.Function;

class CacheCaffeineImpl<K, V> implements Cache<K, V> {
    private final com.github.benmanes.caffeine.cache.Cache<K, V> cache;

    CacheCaffeineImpl(int maxSize) {
        cache = Caffeine.newBuilder()
                .maximumSize(maxSize)
                .build();
    }

    @Override
    public Optional<V> get(K key) {
        return Optional.ofNullable(cache.getIfPresent(key));
    }

    @Override
    public V get(K key, Function<? super K, ? extends V> keyToValueFunction) {
        return cache.get(key, keyToValueFunction);
    }

    @Override
    public void put(K key, V value) {
        cache.put(key, value);
    }

    @Override
    public void invalidate(K key) {
        cache.invalidate(key);
    }

    @Override
    public void invalidateAll() {
        cache.invalidateAll();
    }
}
