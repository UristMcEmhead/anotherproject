package ru.itis.wiki.utils.impl;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import ru.itis.wiki.utils.Cache;
import ru.itis.wiki.utils.CacheManager;

import java.util.HashMap;

@Configuration
class CacheManagerCaffeineImpl implements CacheManager {
    private final Environment env;
    private final HashMap<String, Cache> cacheMap;

    public CacheManagerCaffeineImpl(Environment env) {
        this.env = env;
        cacheMap = new HashMap<>();
    }

    private <K, V> Cache<K, V> createCache(String name) {
        var defaultSize = Integer.parseInt(env.getRequiredProperty("cache.default"));
        var customSizeStr = env.getProperty("cache." + name);
        var customSize = customSizeStr == null ? defaultSize : Integer.parseInt(customSizeStr);

        return new CacheCaffeineImpl<>(customSize);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <K, V> Cache<K, V> getCache(String name) {
        var cache = cacheMap.computeIfAbsent(name, this::createCache);

        return (Cache<K, V>) cache;
    }
}
