package ru.itis.wiki.dao;

import ru.itis.wiki.dto.MindMapNodeDto;
import ru.itis.wiki.models.File;

public interface MindMapDao {

    MindMapNodeDto getNodeFrom(File file);
}
