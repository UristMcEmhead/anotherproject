package ru.itis.wiki.dao.impl;

import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.itis.wiki.dto.MindMapNodeDto;
import ru.itis.wiki.dao.MindMapDao;
import ru.itis.wiki.models.File;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;

@Repository
@AllArgsConstructor
class MindMapDaoImpl implements MindMapDao {
    private static final String SELECT_LINKS =
            "SELECT to_file FROM tag WHERE from_file = ?";

    private final JdbcTemplate jdbcTemplate;

    private Long parseLinks(ResultSet resultSet, int rowNum) throws SQLException {
        return resultSet.getLong(1);
    }

    @Override
    public MindMapNodeDto getNodeFrom(File file) {
        var links = jdbcTemplate.query(SELECT_LINKS, this::parseLinks, file.getId());

        var linksSet = new HashSet<>(links);
        linksSet.remove(file.getId());

        var dir = file.getDirectory();
        var group = dir == null ? 0 : dir.getId();

        return MindMapNodeDto.builder()
                .id(file.getId())
                .name(file.getName())
                .group(group)
                .links(linksSet)
                .build();
    }
}
