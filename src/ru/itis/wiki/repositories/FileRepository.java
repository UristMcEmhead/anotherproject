package ru.itis.wiki.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.wiki.models.Directory;
import ru.itis.wiki.models.File;

import java.util.List;
import java.util.Optional;

public interface FileRepository extends JpaRepository<File, Long> {

    List<File> findAllByDirectoryOrderByName(Directory directory);

    List<File> findAllByDirectory(Directory dir);

    Optional<File> findByNameIgnoreCase(String name);
}
