package ru.itis.wiki.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.wiki.models.File;
import ru.itis.wiki.models.Tag;

public interface TagRepository extends JpaRepository<Tag, Long> {

    void deleteAllByFrom(File from);
}
