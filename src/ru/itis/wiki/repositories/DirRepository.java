package ru.itis.wiki.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.wiki.models.Directory;

import java.util.List;

public interface DirRepository extends JpaRepository<Directory, Long> {

    List<Directory> findAllByParentOrderByName(Directory parent);

    List<Directory> findAllByParent(Directory dir);
}
