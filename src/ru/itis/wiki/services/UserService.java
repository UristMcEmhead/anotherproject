package ru.itis.wiki.services;

import ru.itis.wiki.forms.SignUpForm;

public interface UserService {

    void signUp(SignUpForm form);
}
