package ru.itis.wiki.services;

import ru.itis.wiki.models.File;

public interface FileProcessService {

    String prepareToSave(File file, String text);

    String prepareToEdit(String text);

    String prepareToShow(String text);
}
