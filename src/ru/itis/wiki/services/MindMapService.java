package ru.itis.wiki.services;

import org.springframework.lang.Nullable;
import ru.itis.wiki.dto.MindMapNodeDto;
import ru.itis.wiki.models.Directory;

import java.util.List;

public interface MindMapService {

    List<MindMapNodeDto> getNodes(@Nullable Directory directory);
}
