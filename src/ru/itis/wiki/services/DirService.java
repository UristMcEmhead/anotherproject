package ru.itis.wiki.services;

import ru.itis.wiki.forms.DirForm;
import ru.itis.wiki.models.Directory;
import ru.itis.wiki.models.User;

public interface DirService {

    Directory get(long id);

    Directory save(User owner, DirForm form);

    void delete(Directory dir);
}
