package ru.itis.wiki.services;

import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import ru.itis.wiki.dto.IndexFileDto;
import ru.itis.wiki.models.Directory;

import java.util.List;

@Service
public interface IndexService {

    Pair<List<Directory>, List<IndexFileDto>> getChildrenOf(Directory parent);

    String getPathTo(Directory directory);
}
