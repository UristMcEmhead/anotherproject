package ru.itis.wiki.services.impl;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import ru.itis.wiki.forms.DirForm;
import ru.itis.wiki.models.Directory;
import ru.itis.wiki.models.User;
import ru.itis.wiki.repositories.DirRepository;
import ru.itis.wiki.repositories.FileRepository;
import ru.itis.wiki.services.DirService;

@Service
@AllArgsConstructor
class DirServiceImpl implements DirService {
    private final DirRepository dirRepository;
    private final FileRepository fileRepository;

    @Override
    public Directory get(long id) {
        return dirRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "directory not found")
        );
    }

    @Override
    public Directory save(User owner, DirForm form) {
        var dir = Directory.builder()
                .id(form.getId())
                .name(form.getName())
                .owner(owner)
                .build();

        if (form.getParent() != null) {
            dir.setParent(get(form.getParent()));
        }

        return dirRepository.save(dir);
    }

    @Override
    @Transactional
    public void delete(Directory dir) {
        dirRepository.findAllByParent(dir).forEach(this::delete);

        for (var file : fileRepository.findAllByDirectory(dir)) {
            file.setDirectory(null);
            fileRepository.save(file);
        }

        dirRepository.delete(dir);
    }
}
