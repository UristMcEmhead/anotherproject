package ru.itis.wiki.services.impl;

import lombok.AllArgsConstructor;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import ru.itis.wiki.dto.IndexFileDto;
import ru.itis.wiki.models.Directory;
import ru.itis.wiki.repositories.DirRepository;
import ru.itis.wiki.repositories.FileRepository;
import ru.itis.wiki.services.IndexService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
class IndexServiceImpl implements IndexService {
    private final DirRepository dirRepository;
    private final FileRepository fileRepository;

    @Override
    public Pair<List<Directory>, List<IndexFileDto>> getChildrenOf(Directory parent) {
        var dirs = dirRepository.findAllByParentOrderByName(parent);
        var files = fileRepository.findAllByDirectoryOrderByName(parent)
                .stream()
                .map(IndexFileDto::from)
                .collect(Collectors.toList());

        return Pair.of(dirs, files);
    }

    @Override
    public String getPathTo(Directory directory) {
        var builder = new StringBuilder();
        var dir = directory;

        while (dir != null) {
            builder.insert(0, '/' + dir.getName());
            dir = dir.getParent();
        }

        return builder.toString();
    }
}
