package ru.itis.wiki.services.impl;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.markdownj.MarkdownProcessor;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfig;
import ru.itis.wiki.dto.PopupFileDto;
import ru.itis.wiki.models.File;
import ru.itis.wiki.models.Tag;
import ru.itis.wiki.repositories.FileRepository;
import ru.itis.wiki.repositories.TagRepository;
import ru.itis.wiki.services.FileProcessService;
import ru.itis.wiki.utils.Pair;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
class FileProcessServiceImpl implements FileProcessService {
    private static final Pattern TAG_PATTERN = Pattern.compile("((?:[_\\-a-zA-Zа-яА-Я0-9()'])+)(?:(?:\\[([^]]+)])|(?:\\+(\\S*))|\\|)?");
    private static final Pattern NOT_PROCESSED_TAG = Pattern.compile("(?<!\\\\)@(" + TAG_PATTERN.pattern() + ")");
    private static final Pattern PROCESSED_TAG = Pattern.compile("(?<!\\\\)@(\\d+)");
    private static final Pattern ESCAPED_PROCESSED_TAG = Pattern.compile("(?<!\\\\)&#64;(\\d+)");
    private static final String ESCAPED_TAG_REGEX = "\\\\&#64;";

    private final FileRepository fileRepository;
    private final TagRepository tagRepository;
    private final Template template;
    private final MarkdownProcessor markdownProcessor;
    private final PolicyFactory policy;

    public FileProcessServiceImpl(FileRepository fileRepository, TagRepository tagRepository, FreeMarkerConfig config) {
        this.fileRepository = fileRepository;
        this.tagRepository = tagRepository;
        this.markdownProcessor = new MarkdownProcessor();

        policy = Sanitizers.FORMATTING
                .and(Sanitizers.IMAGES)
                .and(Sanitizers.LINKS)
                .and(Sanitizers.BLOCKS)
                .and(new HtmlPolicyBuilder()
                        .allowElements("pre", "table", "tr", "th", "td")
                        .toFactory());

        try {
            template = config.getConfiguration().getTemplate("util/popupTemplate.ftl");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private String process(String text, Pattern pattern, Function<Matcher, String> f) {
        var matcher = pattern.matcher(text);
        var builder = new StringBuilder();

        while (matcher.find()) {
            matcher.appendReplacement(builder, f.apply(matcher));
        }

        matcher.appendTail(builder);
        return builder.toString();
    }

    private String saveProcessTag(File file, Matcher matcher) {
        var name = matcher.group(2).replace('_', ' ');
        var optional = fileRepository.findByNameIgnoreCase(name);

        var tag = Tag.builder()
                .from(file)
                .to(optional.orElse(null))
                .definition(matcher.group(1))
                .build();

        tagRepository.save(tag);
        return "@" + tag.getId();
    }

    private String processCodeBlocks(String text, Function<String, String> codeBlockLineFunction) {
        var builder = new StringBuilder();
        var isBlock = false;

        for (var line : text.split("\n")) {
            if (line.equals("```")) {
                isBlock = !isBlock;
            } else if (isBlock) {
                line = codeBlockLineFunction.apply(line);
            }

            builder.append(line).append('\n');
        }

        return builder.toString();
    }

    @Override
    public String prepareToSave(File file, String text) {
        var fixedLinebreaks = text.replaceAll("(\r\n)|\r|\n", "\n");
        var escaped = processCodeBlocks(fixedLinebreaks, s -> s.replace("@", "\\@"));

        return process(escaped, NOT_PROCESSED_TAG, matcher -> saveProcessTag(file, matcher));
    }

    private String editProcessTag(Matcher matcher) {
        try {
            var id = Long.parseLong(matcher.group(1));
            var tag = tagRepository.findById(id);

            return tag.map(value -> "@" + value.getDefinition()).orElse("");
        } catch (Exception e) {
            return "";
        }
    }

    @Override
    public String prepareToEdit(String text) {
        var unescaped = processCodeBlocks(text, s -> s.replace("\\@", "@"));
        return process(unescaped, PROCESSED_TAG, this::editProcessTag);
    }

    @Override
    public String prepareToShow(String text) {
        return new TagProcessor().processText(text, true);
    }

    private class TagProcessor {
        // key = tag name (lowercase)
        // value = Pair:
        //     first: full popup (link + popup window)
        //     second: link only
        private final Map<String, Pair<String, String>> tagMap = new HashMap<>();

        private String createPopupTemplate(PopupFileDto dto) {
            try {
                var map = Map.of("file", dto);
                var result = new StringWriter();

                template.process(map, result);

                return result.toString().lines()
                        .map(String::trim)
                        .collect(Collectors.joining());
            } catch (TemplateException | IOException e) {
                throw new IllegalStateException(e);
            }
        }

        private void initPopup(Pair<String, String> templates, String name) {
            var optional = fileRepository.findByNameIgnoreCase(name);

            // create link popup BEFORE full popup
            var linkPopupDto = PopupFileDto.from(name, optional.isPresent());
            var linkTemplate = createPopupTemplate(linkPopupDto);
            templates.setSecond(linkTemplate);

            if (optional.isPresent()) {
                var fullPopupDto = PopupFileDto.from(optional.get());
                fullPopupDto.setDesc(processText(fullPopupDto.getDesc(), false));

                templates.setFirst(createPopupTemplate(fullPopupDto));
            } else {
                // invalid tags do not have full popup
                templates.setFirst(linkTemplate);
            }
        }

        private String processTag(Matcher matcher, boolean showPopup) {
            Optional<Tag> optional;

            try {
                var id = Long.parseLong(matcher.group(1));
                optional = tagRepository.findById(id);
            } catch (Exception e) {
                return matcher.group(0);
            }

            if (optional.isEmpty()) {
                return matcher.group(0);
            }

            var tag = optional.get();
            var tagMatcher = TAG_PATTERN.matcher(tag.getDefinition());

            if (!tagMatcher.matches()) {
                return matcher.group(0);
            }

            var name = tagMatcher.group(1).replace('_', ' ');
            var pair = tagMap.get(name.toLowerCase());

            if (pair == null) {
                pair = new Pair<>();
                tagMap.put(name.toLowerCase(), pair);

                initPopup(pair, name);
            }

            var template = showPopup ? pair.getFirst() : pair.getSecond();
            var text = name;

            if (tagMatcher.group(2) != null) {
                text = tagMatcher.group(2);
            }

            if (tagMatcher.group(3) != null) {
                text += tagMatcher.group(3);
            }

            return template.replaceFirst("%link_text", text);
        }

        private String processCodeBlocks(String markdown) {
            var builder = new StringBuilder();
            var isBlock = false;

            for (var line : markdown.split("\n")) {
                if (line.equals("```")) {
                    isBlock = !isBlock;
                    builder.append('\n');
                } else if (isBlock) {
                    builder.append("    ").append(line);
                } else {
                    builder.append(line);
                }

                builder.append('\n');
            }

            return builder.toString();
        }

        private String markdownToHtml(String markdown) {
            var processed = processCodeBlocks(markdown);
            var html = markdownProcessor.markdown(processed);

            return policy.sanitize(html);
        }

        String processText(String text, boolean showPopup) {
            var html = markdownToHtml(text);

            return process(html, ESCAPED_PROCESSED_TAG, m -> processTag(m, showPopup))
                    .replaceAll(ESCAPED_TAG_REGEX, "@")
                    .replaceAll("</p>\\s*<p>", "\n<br><br>\n")
                    .replace("<p>", "")
                    .replace("</p>", "");
        }
    }
}
