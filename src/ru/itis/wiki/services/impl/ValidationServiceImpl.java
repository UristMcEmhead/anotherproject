package ru.itis.wiki.services.impl;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.itis.wiki.models.User;
import ru.itis.wiki.services.ValidationService;

import javax.servlet.http.HttpSession;
import java.util.Objects;

@Service
class ValidationServiceImpl implements ValidationService {

    @Override
    public User extractFrom(HttpSession session) {
        try {
            return Objects.requireNonNull((User) session.getAttribute("user"));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    public void assertIdEquals(HttpSession session, Long id) {
        if (!extractFrom(session).getId().equals(id)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }
}
