package ru.itis.wiki.services.impl;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import ru.itis.wiki.forms.FileForm;
import ru.itis.wiki.dto.FileDto;
import ru.itis.wiki.dto.IndexFileDto;
import ru.itis.wiki.models.File;
import ru.itis.wiki.models.User;
import ru.itis.wiki.repositories.FileRepository;
import ru.itis.wiki.repositories.TagRepository;
import ru.itis.wiki.services.DirService;
import ru.itis.wiki.services.FileProcessService;
import ru.itis.wiki.services.FileService;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

@Service
@AllArgsConstructor
class FileServiceImpl implements FileService {
    private static final Pattern NAME_PATTERN = Pattern.compile("[ \\-a-zA-Zа-яА-Я0-9()']+");

    private final FileRepository fileRepository;
    private final TagRepository tagRepository;
    private final DirService dirService;
    private final FileProcessService fileProcessService;

    private FileDto toDto(File file) {
        return FileDto.builder()
                .name(file.getName())
                .description(fileProcessService.prepareToShow(file.getDescription()))
                .body(fileProcessService.prepareToShow(file.getBody()))
                .dir(file.getDirectory())
                .ref(URLEncoder.encode(file.getName(), StandardCharsets.UTF_8))
                .owner(file.getOwner())
                .build();
    }

    @Override
    public File get(String name) {
        var fileName = URLDecoder.decode(name, StandardCharsets.UTF_8);
        return fileRepository.findByNameIgnoreCase(fileName).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "file not found")
        );
    }

    @Override
    public File getEditable(String name) {
        var file = get(name);

        file.setDescription(fileProcessService.prepareToEdit(file.getDescription()));
        file.setBody(fileProcessService.prepareToEdit(file.getBody()));

        return file;
    }

    @Override
    public FileDto getDto(String name) {
        return toDto(get(name));
    }

    private boolean isNameValid(String name) {
        return NAME_PATTERN.matcher(name).matches();
    }

    @Override
    public IndexFileDto create(User owner, FileForm form, Long dirId) {
        var optional = fileRepository.findByNameIgnoreCase(form.getName());
        if (optional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "file already exists");
        }

        return save(owner, form, dirId, null);
    }

    @Override
    @Transactional
    public IndexFileDto update(User owner, FileForm form, Long dirId) {
        var optional = fileRepository.findByNameIgnoreCase(form.getName());
        if (optional.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "file does not exist");
        }

        var file = optional.get();
        if (!owner.getId().equals(file.getOwner().getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        tagRepository.deleteAllByFrom(file);
        return save(owner, form, dirId, file.getId());
    }

    @Override
    @Transactional
    public void delete(File file) {
        tagRepository.deleteAllByFrom(file);
        fileRepository.delete(file);
    }

    private IndexFileDto save(User owner, FileForm form, Long dirId, Long fileId) {
        if (!isNameValid(form.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid file name");
        }

        var dir = dirId == null ? null : dirService.get(dirId);
        var file = File.builder()
                .id(fileId)
                .name(form.getName())
                .description("")
                .body("")
                .directory(dir)
                .owner(owner)
                .build();

        // prepareToSave needs file id
        fileRepository.save(file);

        file.setDescription(fileProcessService.prepareToSave(file ,form.getDescription()));
        file.setBody(fileProcessService.prepareToSave(file ,form.getBody()));
        fileRepository.save(file);

        return IndexFileDto.from(file);
    }
}
