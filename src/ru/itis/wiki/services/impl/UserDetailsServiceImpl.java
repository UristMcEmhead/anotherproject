package ru.itis.wiki.services.impl;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.itis.wiki.repositories.UserRepository;
import ru.itis.wiki.security.UserDetailsImpl;

@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        var user = repository.findByName(name).orElseThrow(
                () -> new UsernameNotFoundException(name)
        );

        return new UserDetailsImpl(user);
    }
}
