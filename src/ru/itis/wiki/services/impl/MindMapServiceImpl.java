package ru.itis.wiki.services.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.wiki.dto.MindMapNodeDto;
import ru.itis.wiki.models.Directory;
import ru.itis.wiki.models.File;
import ru.itis.wiki.repositories.DirRepository;
import ru.itis.wiki.repositories.FileRepository;
import ru.itis.wiki.dao.MindMapDao;
import ru.itis.wiki.services.MindMapService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
class MindMapServiceImpl implements MindMapService {
    private final DirRepository dirRepository;
    private final FileRepository fileRepository;
    private final MindMapDao mindMapDao;

    private void addFiles(Directory directory, List<File> files) {
        dirRepository.findAllByParent(directory)
                .forEach(dir -> addFiles(dir, files));

        files.addAll(fileRepository.findAllByDirectory(directory));
    }

    @Override
    public List<MindMapNodeDto> getNodes(Directory directory) {
        var files = new ArrayList<File>();
        addFiles(directory, files);

        return files.stream()
                .map(mindMapDao::getNodeFrom)
                .collect(Collectors.toList());
    }
}
