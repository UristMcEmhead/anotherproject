package ru.itis.wiki.services.impl;

import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.wiki.forms.SignUpForm;
import ru.itis.wiki.models.User;
import ru.itis.wiki.repositories.UserRepository;
import ru.itis.wiki.services.UserService;

import java.util.Objects;
import java.util.regex.Pattern;

@Service
@AllArgsConstructor
class UserServiceImpl implements UserService {
    private static final Pattern USERNAME_PATTERN = Pattern.compile("[_a-zA-Z]+");

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void signUp(SignUpForm form) {
        var name = Objects.requireNonNull(form.getUsername(), "username is null");
        var password = Objects.requireNonNull(form.getPassword(), "password is null");
        var passwordAgain = Objects.requireNonNull(form.getPasswordAgain(), "passwordAgain is null");

        if (!USERNAME_PATTERN.matcher(name).matches()) {
            throw new IllegalArgumentException("invalid username");
        }

        if (userRepository.findByName(name).isPresent()) {
            throw new IllegalArgumentException("username is already taken");
        }

        if (!password.equals(passwordAgain)) {
            throw new IllegalArgumentException("passwords do not match");
        }

        var user = User.builder()
                .name(name)
                .passHash(passwordEncoder.encode(password))
                .build();

        userRepository.save(user);
    }
}
