package ru.itis.wiki.services;

import ru.itis.wiki.models.User;

import javax.servlet.http.HttpSession;

public interface ValidationService {

    User extractFrom(HttpSession session);

    void assertIdEquals(HttpSession session, Long id);
}
