package ru.itis.wiki.services;

import org.springframework.transaction.annotation.Transactional;
import ru.itis.wiki.forms.FileForm;
import ru.itis.wiki.dto.FileDto;
import ru.itis.wiki.dto.IndexFileDto;
import ru.itis.wiki.models.File;
import ru.itis.wiki.models.User;

public interface FileService {

    File get(String name);

    File getEditable(String name);

    FileDto getDto(String name);

    IndexFileDto create(User owner, FileForm form, Long dirId);

    @Transactional
    IndexFileDto update(User owner, FileForm form, Long dirId);

    void delete(File file);
}
