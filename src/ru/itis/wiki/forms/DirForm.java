package ru.itis.wiki.forms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DirForm {
    private Long id;
    private Long parent;
    private String name;
}
