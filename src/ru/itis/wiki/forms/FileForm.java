package ru.itis.wiki.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.wiki.models.File;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FileForm {
    private String name;
    private String description;
    private String body;

    public static FileForm from(File file) {
        return FileForm.builder()
                .name(file.getName())
                .body(file.getBody())
                .description(file.getDescription())
                .build();
    }
}
