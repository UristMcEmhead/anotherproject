package ru.itis.wiki.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
@AllArgsConstructor
public class MindMapNodeDto {
    private Long id;
    private String name;
    private Long group;
    private Set<Long> links;
}
