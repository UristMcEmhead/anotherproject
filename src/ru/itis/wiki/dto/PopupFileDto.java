package ru.itis.wiki.dto;

import lombok.Builder;
import lombok.Data;
import ru.itis.wiki.models.File;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Data
@Builder
public class PopupFileDto {
    private String ref;
    private String name;
    private String desc;
    private boolean valid;

    public static PopupFileDto from(File file) {
        return PopupFileDto.builder()
                .ref(URLEncoder.encode(file.getName(), StandardCharsets.UTF_8))
                .name(file.getName())
                .desc(file.getDescription())
                .valid(true)
                .build();
    }

    public static PopupFileDto from(String name, boolean valid) {
        return PopupFileDto.builder()
                .ref(URLEncoder.encode(name, StandardCharsets.UTF_8))
                .name(name)
                .valid(valid)
                .build();
    }
}
