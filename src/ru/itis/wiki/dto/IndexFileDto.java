package ru.itis.wiki.dto;

import lombok.Builder;
import lombok.Data;
import ru.itis.wiki.models.File;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Data
@Builder
public class IndexFileDto {
    private String ref;
    private String name;

    public static IndexFileDto from(File file) {
        return IndexFileDto.builder()
                .ref(URLEncoder.encode(file.getName(), StandardCharsets.UTF_8))
                .name(file.getName())
                .build();
    }
}
