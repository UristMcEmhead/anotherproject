package ru.itis.wiki.dto;

import lombok.Builder;
import lombok.Data;
import ru.itis.wiki.models.User;

@Data
@Builder
public class UserDto {
    private Long id;
    private String username;

    public static UserDto from(User user) {
        return builder()
                .id(user.getId())
                .username(user.getName())
                .build();
    }
}
