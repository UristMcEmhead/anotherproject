package ru.itis.wiki.dto;

import lombok.Builder;
import lombok.Data;
import ru.itis.wiki.models.Directory;
import ru.itis.wiki.models.User;

@Data
@Builder
public class FileDto {
    private String ref;
    private String name;
    private String description;
    private String body;
    private Directory dir;
    private User owner;
}
