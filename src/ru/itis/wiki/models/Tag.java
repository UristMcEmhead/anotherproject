package ru.itis.wiki.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "from_file", nullable = false)
    private File from;

    @ManyToOne
    @JoinColumn(name = "to_file")
    private File to;

    @Column(nullable = false)
    private String definition;
}
