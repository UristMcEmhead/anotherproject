package ru.itis.wiki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import ru.itis.wiki.utils.CookieToSessionFilter;

import java.util.List;

@SpringBootApplication
public class WikiMapApplication {

    @Bean
    public FilterRegistrationBean cookieToSessionFilter() {
        var registration = new FilterRegistrationBean<CookieToSessionFilter>();
        registration.setFilter(new CookieToSessionFilter());
        registration.setUrlPatterns(List.of("/*"));

        return registration;
    }

//    @Configuration
//    static class MyConfig extends WebMvcConfigurationSupport {
//        @Override
//        public void addFormatters(FormatterRegistry registry) {
//            registry.addFormatter(new TranslateFormatter());
//        }
//
//    }

    public static void main(String[] args) {
        SpringApplication.run(WikiMapApplication.class, args);
    }
}
