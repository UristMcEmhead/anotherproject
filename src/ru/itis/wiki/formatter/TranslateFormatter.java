package ru.itis.wiki.formatter;

import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.format.Formatter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.Locale;
import java.util.Map;

@Slf4j
public class TranslateFormatter implements Formatter<String> {
    private OkHttpClient httpClient = new OkHttpClient();
    private JsonParser springParser = JsonParserFactory.getJsonParser();

    @NotNull
    @Override
    public String parse(String s, Locale locale) throws ParseException {
        try {
            return doTranslate(s,locale);
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage());
        }
        return "";
    }

    @NotNull
    @Override
    public String print(String s, Locale locale) {
        try {
            String r = doTranslate(s,locale);
            log.info(r);
            return r;
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage());
        }
        return "";
    }

    private String doTranslate(String msg, Locale locale) throws UnsupportedEncodingException {
        String result = "text";
        RequestBody formBody = new FormBody.Builder()
                .add("text", URLEncoder.encode(msg, "UTF-8"))
                .build();

        Request request = new Request.Builder()
                .url("https://translate.yandex.net/api/v1.5/tr.json/translate?lang=" + locale.getLanguage() + "&key=trnsl.1.1.20200418T155719Z.369fdfa79d23827d.9984388a3a5c94cf9ee2041f3b7b5337087015f5")
                .post(formBody)
                .build();
        try (Response response = httpClient.newCall(request).execute()) {

            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            result = parseJSON(response.body().string());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return result;
    }

    private String parseJSON(String string){
        Map<String, Object> map = springParser.parseMap(string);
        return map.get("text").toString();
    }
}
